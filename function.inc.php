<?php
abstract class cleandata {
    protected $table;
    protected $field;
    protected $id;
    protected $results;
    public function __construct( $table, $field, $id) {
        $this->table = $table;
        $this->field = $field;
        $this->id = $id;
        return self::mysqlquery();
    }
    public function mysqlquery() {
        global $wpdb;
        $results = $wpdb->query("DELETE FROM $this->table WHERE $this->field = $this->id");
        $this->results = $results;
        if(is_numeric($this->results)) {
            return sprintf('Deleted ID: %s data from table %s. Number of data deleted: %s',$this->id, $this->table, $this->results);
        } else {
            return sprintf('Deleted ID: %s . There is no Table: %s or Field: %s the Result: %s',$this->id, $this->table, $this->field, $this->results);
        }
    }
    abstract protected function results();
    public function __destruct() {}
}
class deletethis extends cleandata {
    protected $results;
    public function __construct( $table, $field, $id) {
        $this->results = parent::__construct( $table, $field, $id);
        self::writelog();
    }
    public function writelog() {
        $path = dirname(__FILE__) . '/log/log.txt';
        if (($h = fopen($path, "a")) !== FALSE) {
            $mystring = 'Date: '. date("Y-m-d H:i:s") .PHP_EOL. $this->results .PHP_EOL.PHP_EOL;
            fwrite( $h, $mystring );
            fclose($h);
        }
        else
            die('WHAT IS GOING ON?');
    }
    public function results() {
        return $this->results;
    }
    public function __destruct() {}
}


?>