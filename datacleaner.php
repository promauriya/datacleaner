<?php
/**
 * @package Automatic Data Cleaner
 */
/*
Plugin Name: Automatic Data Cleaner
Plugin URI: https://efhutton.com/
Description: Cleaning other user data when a  user is deleted. 
Version: 1.0
Author: EFHutton::Ryan
Author URI: https://efhutton.com
License: GPLv2 or later
Text Domain: Cleaning other user data when a  user is deleted.
*/
add_action('admin_menu', 'efhuton_datacleaner');
function efhuton_datacleaner() {
    $page_title = 'Automatic Data Cleaner';
    $menu_title = 'Automatic Data Cleaner';
    $capability = 'edit_posts';
    $menu_slug = 'efhtn_autodatacleaner';
    $function = 'efhtn_autodatacleaner';
    $icon_url = plugins_url( 'images/datacleaner.png', __FILE__ );
    $position = 25;

    add_menu_page( $page_title, $menu_title, $capability, $menu_slug, $function, $icon_url, $position );
}
/* Determine The Time Location START */
date_default_timezone_set("America/New_York");
define('CLEANER_DIR', plugin_dir_path( __FILE__ ));
//define('THE_RYNTOK', mysecretryn());
/* Determine The Time Location END */
function efhtn_autodatacleaner() {
  	
	include( 'page/page.php');
  
}
// $a = new deletethis('wp_posts','post_author',1);
// echo $a->results();
add_action( 'deleted_user', 'deleteuserdata' );
function deleteuserdata($user_id) {
	$tabledata = array (
					array("table" => "wp_pmpro_memberships_users", "field" => "user_id"),
					array("table" => "wp_15_bp_activity", "field" => "user_id"),
					array("table" => "wp_myCRED_log", "field" => "user_id"),
					array("table" => "wp_bp_notifications", "field" => "user_id"),
					array("table" => "wp_bp_messages_recipients", "field" => "user_id"),
					array("table" => "wp_bp_messages_messages", "field" => "sender_id")
				   );
	foreach ($tabledata as &$value) {
		$a = new deletethis($value['table'],$value['field'],$user_id);
	}
}
include( CLEANER_DIR. 'function.inc.php');
?>